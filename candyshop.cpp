/**
 * Candyshop v1.0
 * Author: Raul Torres
 * 2019
 */ 

// includes
#include <iostream>
#include <string_view>
#include <string>
#include <map>
#include <memory>
#include <omp.h>
#include <time.h>
#include "randomhelper.h"

// namespaces
using namespace std;

// typedefs for maps and entries
typedef pair <string, int> ValueAndHit;
typedef map <string, ValueAndHit > CandyMap; // something like a table of the form: {key, value, hit counter}
typedef pair <string, ValueAndHit > CandyMapEntry; 


// struct for parameter handling
typedef struct 
{
    string operation;
    string key;
    string value;
} Parameters;

/**
 * Enclosing class
 */
class Candyshop
{
    private: // members
        
    // OpenMP shared variables are labeled as such to ease the understanding of the parallel sections
    
    vector<string> operations = {"PUT", "GET", "DELETE"}; // list of allowed operations
    int mSharedHitThreshold = 0; // entries above the threshold stay in the L1 cache, while entries of the L2 cache are moved to the disk
    int mSharedGETCacheHits = 0; // total cache hits when using GET
    int mSharedGETCacheMisses = 0; // total cache misses when using GET
    int mSharedLnCacheTotalSize = 0; // maximum combined number of entries allowed in L1 and L2 cache
    int mSharedGreatestHit = 0; // the highest hit number an entry has received since the last time the threshold was reset
    int mSharedNumThreads = 1; // for info
    
    // the maps
    CandyMap mSharedL1Cache; // contains the popular entries
    CandyMap mSharedL2Cache; // the default map where everything is inserted
    shared_ptr<CandyMap> mSharedVirtualStorage = make_shared<CandyMap>(); // simulation of the disk 

    private: // methods
    
    /**
     * print welcome screen
     */ 
    void printWelcomeScreen() const
    {
        cout << "Candyshop v1.0"<< endl;
        cout << "Author: Raul Torres"<< endl << endl;
    }    
    
    /**
     * print some useful info
     */ 
    void printSizes() const
    {
        cout << "L1 cache size: "<< mSharedL1Cache.size() << " entries" << endl;
        cout << "L2 cache size: "<< mSharedL2Cache.size() << " entries" << endl;
        cout << "Virtual storage size: "<< mSharedVirtualStorage->size() << " entries" << endl;
    }
    
    /**
     * print some statistics
     */ 
    void printStatistics() const
    {
        int totalGets = mSharedGETCacheHits + mSharedGETCacheMisses;
        if(totalGets > 0)
        {
            cout << "Hit threshold: " << mSharedHitThreshold << endl;
            cout << "Greatest hit: " << mSharedGreatestHit << endl;
            cout << "GET cache hits: "<< (float)mSharedGETCacheHits / totalGets << " %" << endl;
            cout << "GET cache misses: "<< (float)mSharedGETCacheMisses / totalGets << " %" << endl;
            printSizes();
        }
    }
        
    /**
     * Eviction strategy
     */
    void restructCaches()
    {
        // for info
        cout << "Before restruct:" << endl;
        printSizes();
        
        // clear the L2 cache and send all its entries to the virtual storage
        for(CandyMapEntry entry : mSharedL2Cache)
        {
            entry.second.second = 0; // reset hit counter, as in virtual storage, an entry loses all hit history, this prevents old hits to come back from forgiveness with a single recent hit
            mSharedVirtualStorage->insert(entry);
            mSharedL2Cache.erase(entry.first);
        }
        
        // find the less popular hit number in L1 cache and make it the new threshold
        mSharedHitThreshold = mSharedGreatestHit;
        for(const CandyMapEntry& entry : mSharedL1Cache)
        {
            if(entry.second.second < mSharedHitThreshold)
            {
                mSharedHitThreshold = entry.second.second;
            }
        }
        
        // move all entries in or below the threshold from L1 to L2
        for(CandyMapEntry entry : mSharedL1Cache)
        {
            if (entry.second.second <= mSharedHitThreshold) // entry.second.second a.k.a hit counter
            {
                mSharedL2Cache.insert(entry);
                mSharedL1Cache.erase(entry.first); // entry.first a.k.a key
            }
        }
        
        // reset hit counter and threshold if both caches are empty
        if(mSharedL1Cache.empty() && mSharedL2Cache.empty())
        {
            mSharedHitThreshold = 0; // reset the threshold
            mSharedGreatestHit = 0; // reset the highest hit
        }
        
        cout << "After restruct:" << endl;
        printStatistics();
    }
    
    /**
     * the caches are full if we insert a new entry?
     */ 
    bool isTotalSizeOverflown() const
    {
        if(mSharedLnCacheTotalSize < mSharedL1Cache.size() + mSharedL2Cache.size() + 1)
        {
            return true;
        }
        return false;
    }
        
    /**
    * The operation that puts data, which goes automatically to L2 cache
    */
    string putOperation(string_view key, string_view value)
    {
        string retcode = "ERROR:KEY_EXISTS";
        #pragma omp critical (candyshop_lock) // all critical regions with this name act like a single mutex area
        {
            if(mSharedL1Cache.find(key.data()) == mSharedL1Cache.end()) // not in L1 cache
            {
                if(mSharedL2Cache.find(key.data()) == mSharedL2Cache.end()) // not in L2 cache
                {
                    if(mSharedVirtualStorage->find(key.data()) == mSharedVirtualStorage->end()) // not in virtual storage
                    {
                        while(isTotalSizeOverflown()) // we reached the entry limit
                        {
                            restructCaches(); 
                        }
                        ValueAndHit aPair = make_pair(value.data(), 0);
                        mSharedL2Cache.insert(make_pair(key.data(), aPair)); // all inserts happen in L2 cache
                        retcode = "SUCCESS:KEY_INSERTED";
                    }
                }
            }
        }
        return retcode;
    }

    /**
    * The operation that gets data
    */
    string getOperation(string_view key)
    {
        string retvalue = "ERROR:KEY_NOT_FOUND";
        #pragma omp critical (candyshop_lock) // all critical regions with this name act like a single mutex area
        {
            CandyMap::iterator entryitL1 = mSharedL1Cache.find(key.data());
            if(entryitL1 != mSharedL1Cache.end())// found in L1 cache
            {
                retvalue = "SUCCESS:KEY_RETRIEVED:" + entryitL1->second.first; // entryitL1->second.first a.k.a value
                entryitL1->second.second++; // increment the hit counter for this entry
                mSharedGETCacheHits++; // and the hit statistics
                if(entryitL1->second.second > mSharedGreatestHit) // update the highest hit
                {
                    mSharedGreatestHit = entryitL1->second.second;
                }
            }
            else // L2 or virtual storage
            {
                CandyMap::iterator entryitL2 = mSharedL2Cache.find(key.data());
                if(entryitL2 != mSharedL2Cache.end()) // found in L2 cache
                {
                    retvalue = "SUCCESS:KEY_RETRIEVED:" + entryitL2->second.first; // get the value
                    entryitL2->second.second++; // increment the hit counter for this entry
                    mSharedGETCacheHits++; // and the hit statistics
                    if(entryitL2->second.second > mSharedGreatestHit) // update the highest hit
                    {
                        mSharedGreatestHit = entryitL2->second.second;
                    }
                    
                    // if an entry is above the threshold, move it to the L1 cache
                    if(entryitL2->second.second > mSharedHitThreshold)
                    {
                        mSharedL1Cache.insert(*entryitL2);
                        mSharedL2Cache.erase(entryitL2);
                    }
                }
                else // virtual storage
                {
                    // if found in virtual storage, we move it back to L2 cache with hit counter in 0
                    CandyMap::iterator entryitStorage = mSharedVirtualStorage->find(key.data());
                    if(entryitStorage != mSharedVirtualStorage->end())
                    {
                        retvalue = "SUCCESS:KEY_RETRIEVED:" + entryitStorage->second.first; // get the value
                        mSharedGETCacheMisses++; // update the statistics
                        while(isTotalSizeOverflown()) // we reached the entry limit
                        {
                            restructCaches();
                        }
                        mSharedL2Cache.insert(*entryitStorage);
                        mSharedVirtualStorage->erase(entryitStorage);
                    }
                }
            }
        }
        return retvalue;
    }

    /**
    * The operation that deletes data
    */
    string deleteOperation(string_view key)
    {
        string retvalue = "ERROR:KEY_NOT_FOUND";
        #pragma omp critical (candyshop_lock) // all critical regions with this name act like a single mutex area
        {
            if(mSharedL1Cache.find(key.data()) != mSharedL1Cache.end()) // found in L1 cache
            {
                mSharedL1Cache.erase(key.data());
                retvalue = "SUCCESS:KEY_DELETED";
            }
            else if(mSharedL2Cache.find(key.data()) != mSharedL2Cache.end()) // found in L2 cache
            {
                mSharedL2Cache.erase(key.data());
                retvalue = "SUCCESS:KEY_DELETED";
            }
            else if(mSharedVirtualStorage->find(key.data()) != mSharedVirtualStorage->end()) // found in virtual storage
            {
                mSharedVirtualStorage->erase(key.data());
                retvalue = "SUCCESS:KEY_DELETED";
            }
        }
        return retvalue;
    }

    /**
    * The task that runs in parallel for each thread, no need for mutex structures yet
    */
    void task(const Parameters &taskParameters)
    {
        string outcome;
        if(taskParameters.operation == "PUT")
        {
            outcome = putOperation(taskParameters.key, taskParameters.value);
        }
        
        else if(taskParameters.operation == "GET")
        {
            outcome = getOperation(taskParameters.key);
        }
        else if(taskParameters.operation == "DELETE")
        {
            outcome = deleteOperation(taskParameters.key);
        }
        else
        {
            outcome = "ERROR:UNSUPPORTED_OPERATION";
        }
        cout << "Thread " << omp_get_thread_num() + 1 << " of " << mSharedNumThreads <<  " -> " << taskParameters.operation <<  " -> " << outcome << endl;
    }
    
    public: /// methods
    
    /**
    * Class unique entry point
    */
    void run(int limit) 
    {
        // hello!
        printWelcomeScreen();
        
        // initialize variables and configurations
        mSharedLnCacheTotalSize = limit;
        srand ( time(NULL) ); //initialize the random seed
        const auto ch_set = charset();
        default_random_engine rng(random_device{}());  // create a non-deterministic random number generator (credits: https://stackoverflow.com/users/13760/carl) 
        uniform_int_distribution<> dist(0, ch_set.size()-1); // create a random number "shaper" that will give us uniformly distributed indices into the character set (credits: https://stackoverflow.com/users/13760/carl)
        auto randchar = [ ch_set,&dist,&rng ](){return ch_set[ dist(rng) ];};  // reate a function that ties them together, to get: a non-deterministic uniform distribution from the character set of your choice (credits: https://stackoverflow.com/users/13760/carl)

        // get the number of threads, for printing purposes
        #pragma omp parallel // next instruction hast to be in a parallel context in order to return a meaningful value
        #pragma omp single // but use only one thread to get the value
        mSharedNumThreads = omp_get_num_threads();
        
        // force a repeated entry, to test how it moves bewtween caches
        Parameters forcedEntryParameters = {"PUT", "aaaaaaaaaaaaaa", "bbbbbbbbbbbbbbb"};
        task(forcedEntryParameters);
        forcedEntryParameters.operation = "GET";
        
        // the processing cycle
        #pragma omp parallel // start a parallel region, without it, it is not possible to spawn parallel threads
        #pragma omp single // but let only one thread spawn each new task. "single" has an implicit barrier, so all threads must have finished before going beyond the "while"
        while(true)
        {
            // randomize some values
            int randOpIndex = rand() % 3; //generates a random number between 0 and 2
            int stringLength = rand() % 9; // set the length of the string you want and profit! (credits: https://stackoverflow.com/users/13760/carl)
            
            // generate some random strings and operations
            Parameters taskParameters = {operations[randOpIndex], random_string(stringLength, randchar), ""};
            if(taskParameters.operation == "PUT")
            {
                taskParameters.value = random_string(stringLength,randchar);
            }
    
            // the task for the random generated strings
            #pragma omp task default(none) firstprivate(taskParameters) // we make sure each task has its own parameters initialized
            task(taskParameters);
            
            // the forced GET task that repeats to simulated a very popular entry
            #pragma omp task default(none) firstprivate(forcedEntryParameters) // we make sure each task has its own parameters initialized
            task(forcedEntryParameters);
        }
    }
};

/**
 * Program entry point, it needs one argument, which is the total combined number of allowed entries for L1 and L2 caches
 * 
 * e.g.
 * 
 * candyshop.exe 10000
 */
int main(int argc, char **argv) 
{
    // very basic argument validation
    if(argc < 2)
    {
        cout << "Argument 'limit' is missing" << endl;
        return 0;
    }
    if (argv[1] == nullptr)
    {
        cout << "Argument 'limit' is missing" << endl;
        return 0;
    }
    int limit = strtol(argv[1], nullptr, 10);
    
    // instantiate
    Candyshop myCandyShop;
    
    // run
    myCandyShop.run(limit);
    
    // good bye
    return 0;
}
