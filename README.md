## Candy Shop Simulator

Imagine you have candy shop and you want to keep exhibited those candies that are the favorite for the people, and rotate those which are not.
This is just a metaphor for a program that could store keys and values in memory in an efficient way.
I simulate this with a C++ data structure (std::map) in memory that store a character-based key (the candy barcode), 
a character-based value (the candy name) and an integer value (the hit counter).

Here by my humble implementation. 
Please, feel free to improve it, critizice it, comment it, destroy it or use it (under your own risk). 
Purposes are merely educational and recreational.

### Algorithm description

We assume that the system has been operating for a while already, and the amount of entries saved in the virtual storage are significantly greater than the ones in the caches.

The idea is to optimize the cache usage. For that, we use std::map whose complexity is a guaranteed O(log(n)), where n refers to the number of entries. I decided not to use an unordered map because the efficiency depends on the hash function, and this changes from compiler to compiler. A bad hash function can make rise the complexity to O(n) if a bucket ends up filled up more than the others.

The cache is organized in two levels: L1 and L2 cache. The total size of the cache is the summation of the size of both caches. L2 cache is the place where new elements are inserted and the place where entries from the virtual storage are saved when retrieved. This is the cache that gets emptied when we call our eviction algorithm. L1 cache is the place where the popular entries are stored. Popularity is measured with a per-entry field called "hit counter".  During eviction, entries in L1 cache whose hit counter is equal to a hit threshold are moved back to the L2 cache.

We use OpenMP Tasks for parallelizing the interaction with the clients. For each connected client, a new task will be spawn. Anytime a client needs to interact with the state of the system (namely, access the map or the virtual storage, which are shared among the threads), it enters into an OMP critical region, which means, the client thread has exclusive access to this part of the code, while the other threads wait. This prevents race conditions and allows the system to have a well determined state before introducing changes or reading data from it.

The conditionals in the code are organized in such form that the in-memory checks precede the in-disk checks, so we can avoid the best we can the inherent slowdown of disk access. 

When the operation is "DELETE", we check first in the caches and then in the virtual storage, if the key exists and we delete the entry. No big deal.

When the operation is "INSERT", the key and the value are stored into the L2 cache, provided there is no occurrence of the key neither in cache or in virtual storage. We reintroduce here the hit counter. This variable keeps track of the number of times that a key-value tuple has been requested via GET. When a tuple is inserted, this attribute is initialized with the value zero.

When the operation is "GET", the key is searched first in the caches and then in the virtual storage. If we found it in the caches, we increment by one the hit counter of this particular entry. If this entry's hit counter surpases a given hit threshold, the entry is moved to the L1 cache. If the entry was found in the virtual storage, we move the entry to the L2 cache with the hit counter set to zero.

So, in practice, the last two operations might insert data into the caches (INSERT does it always, GET does it only when the tuples are in virtual storage). What happens then if the caches have reached the maximum size? Here is where we come with the eviction strategy.

Every time the cache limit is reached, we move all the entries in the L2 cache into the virtual storage. All entry's hit counter in L2 cache are minor or equal to the hit threshold value. At the beginning, the hit threshold value is zero, which means, we evict all the entries which were never requested via GET. After that, we increase our threshold in one unit, so, next time the cache is full, we get rid of the entries with 1 or less accesses via GET. And so on.

The idea behind is to give preference in the L1 cache to those entries which are being constantly accessed. Imagine a candy store where a certain chocolate bombon is the favorite of the customers. Each time the candy is bought, its hit counter increases, maybe more rapidly than the hit threshold, which guarantees it will stay in the window front (L1 cache) as long as it is popular. For candies not keeping up with the popularity, the system will keep them ready for eviction in L2 cache.

Why increasing the threshold? Cause on the long run, there might be a huge difference between the most accessed entries and the less popular ones, so we want to keep in cache only those who are frequently requested and have the space free for introducing new entries. Going back to the candy store analogy: we want to keep showing our best selling bombons, but at the same time, having room to present new sweets that might attract the attention of the customer. 

What happens when both caches (L1 and L2) are completely empty? We reset the threshold to zero, and start all over again. We don't want to store the number of requests on the virtual storage, because once we retrieve an entry we might have one case of a tuple that was popular (frequently requested) some far time ago. So, the threshold will have to reach that number again in order to put it out from the cache. We don't want that, we want to focus on the in-memory access, which means, we want to focus on the most recent operations.

Now, if we convert the virtual storage into a L3 cache, then it would make sense to preserve the hit counter. But this is not in the scope of the current implementation.


### Next improvements

* ~~Change the "while" loop in the eviction strategy for a search of the minor hit counter in the L1 cache, which reduces to O(n) the complexity of the eviction of entries from L1 to L2.~~
* Disk I/O : smartly move the virtual storage in batch, to reduce the overhead of per-thread disk access.
* L3 cache? Dynamic multi-level cache?

